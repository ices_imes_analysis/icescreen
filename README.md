ICEscreen is a bioinformatic pipeline for the detection and annotation of ICEs (Integrative and Conjugative Elements) and IMEs (Integrative and Mobilizable Elements) in Bacillota genomes. 

**Main features of ICEscreen**

- Detection of signature proteins (SPs) of ICEs/IMEs by using blastP on a curated resource.	BlastP allows for an accurate assignment of hits to a given ICE/IME superfamily or family. The curated resource was derived from an analysis of over 120 ICEs and IMEs in Streptococcus genomes by the DynAMic lab at Nancy.
- Detection of distant homologs of SPs by using HMM profiles of ICEs/IMEs protein families. The HMM profiles have been either imported from trusted resources or created and curated when needed.
- Detection of the ICE/IME structures: ICEScreen groups together SPs that belong to the same ICE/IME structures to the best of its ability.
- Delimitation of the elements at the gene or nucleotide level is not yet implemented and still needs manual curation.

**Main Output files**

There are 3 main output results files generated by ICEscreen:

- Detected Signature Proteins table (`*_detected_SP_withMEIds.csv`): list of the signature proteins detected by the tool and their possible assignment to an ICE or IME element. It is a comma separated table of 48 columns with a one line header. Each line represents a signature protein detected by ICEscreen.
- Detected ICEs/IMEs table (`*_detected_ME.tsv`): list of the ICEs and IMEs elements detected by the tool, including information about the signature proteins they contain. It is a tab separated table of 21 columns, the header is at line #3. Information in this file is similar to the output file _withICEIMEIds.csv (option -m) but centered around a list of ICE / IME structures instead of a list of SPs.
- Results summary (`*_detected_ME.summary`): this file summarizes the main parameters and statistics regarding the ICE / IME structures and the SPs.

**Documentation and Usage**

The full documentation is available at https://icescreen.migale.inrae.fr.

**Citation**

ICEscreen is developed by J. Lao, T. Lacroix, C. Coluzzi, G. Guédon, N. Leblond-Bourget and H. Chiapello from the University of Lorraine DynAMic and the INRAE MaIAGE research teams. Thank you for citing our latest publication, see https://icescreen.migale.inrae.fr for details.

**External links**

- Software heritage hal INRAE: https://hal.archives-ouvertes.fr/hal-03610196
- DOI: https://doi.org/10.57745/EV2ZHI (https://entrepot.recherche.data.gouv.fr/dataset.xhtml?persistentId=doi:10.57745/EV2ZHI)
- INRAE digital asset: https://actif-numerique.inrae.fr/ansci/app/systeme-information/810

