#!/bin/bash
#set -euo pipefail

########################################
# FUNCTIONS
########################################
# Usage function (show help).

function version(){
    printf "1.3.3\n"
}

function usage(){
    printf "\nICEscreen version 1.3.3\n\n"
    printf "* Command line for running an analysis with ICEscreen:\n\ticescreen [-o OUTDIR] [-n ANALYSIS_NAME] [-j NB_JOBS] -p TAXONMIC_PHYLUM -g GBDIR\n"

    printf "\n* Mandatory arguments for running icescreen:\n"
    printf "\t-g, --gbdir                 : Path to the folder containing the genbank files (Mandatory). Multigenbank files are supported. Each Genbank file must include the ORIGIN nucleotide sequence at the end.\n"
    
    printf "\n* Soon to be mandatory arguments for running icescreen:\n"
    printf "\t-p, --phylum                : Phylum of the organisms to analyse. Supported phylum: \"bacillota\".\n"

    printf "\n* Optional arguments for running icescreen:\n"
    printf "\t-o, --outdir                : Path to where ICEscreen results will be written (default "${HOME}").\n"
    printf "\t-n, --name                  : Name of the analysis (default None).\n"
    printf "\t-j, --jobs                  : Maximum number of processes running in parallel (default 1).\n"
    printf "\t--galaxy                    : Do not try to activate the default icescreen_env Conda environment before running icescreen. This option allows to run ICEscreen on a Galaxy instance.\n"

    printf "\n* Other arguments (admin, settings, help, etc.):\n"
    printf "\t-h, --help                  : Show this message and exit.\n"
    printf "\t-v, --version               : Show the version of ICEscreen and exit.\n"
    printf "\t--install_dependencies      : Uninstall and re-install the dependencies of ICEscreen via Conda.\n"
    printf "\t--print_version_dependencies: Show the version of the dependencies for ICEscreen and exit.\n"
    printf "\t--index_genomic_resources   : Index or re-index the genomic resources related to the signature proteins used by ICEscreen (proteins fasta via makeblastdb and HMM profiles via hmmpress) and exit.\n"
    printf "\t--test_installation         : Test whether the process of installing ICEscreen was successful and exit. If so, the sentence \"The installation of ICEscreen is successful\" will appear.\n"


}

# die function (show message then quit).
function die() {
    printf '%s\n' "$1" >&2
    exit 1
}


exit_if_error() {
  local exit_code=$1
  shift
  [[ $exit_code ]] &&               # do nothing if no error code passed
    ((exit_code != 0)) && {         # do nothing if error code is 0
      printf 'ERROR: %s\n' "$@" >&2 # we can use better logging here
      exit "$exit_code"             # we could also check to make sure
                                    # error code is numeric when passed
    }
}

function get_abs_filename(){
  # $1 : relative filename
  # echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
  echo "$(cd "$(dirname -- "$1")" >/dev/null; pwd -P)/$(basename -- "$1")"
}


function install_dependencies(){
	# Installation of ICEscreen conda environement from YAML
	conda env remove -n icescreen_env_1-3-3
	conda env create -f "$(dirname $(readlink -f $0))"/icescreen_envs/icescreen_env_1-3-3.yml
	exit_if_error $? "conda env create failed"
}


function index_genomic_resources(){
        local galaxy_code_sent=$1
	conda_activate_icescreen_env "${galaxy_code_sent}"

	for filename in "${Icescreen_root_dir}"/icescreen_detection_SP/database/blastdb/*.faa "${Icescreen_root_dir}"/icescreen_detection_SP/database/blastdb/*.fa "${Icescreen_root_dir}"/icescreen_detection_SP/database/blastdb/*.fasta; do
	    echo "${filename}"
	    if [ -f "${filename}" ]; then
		dirnameFaaFile=$(dirname "${filename}")
		titleName=$(basename "${filename}")
		suffixOutName=$(basename "${filename}" | rev | cut -f 2- -d '.' | rev) # remove extension of the file
		outName="${dirnameFaaFile}/${suffixOutName}"
		makeblastdb -in "${filename}" -input_type fasta -dbtype prot -title "${titleName}" -out "${outName}"
		exit_if_error $? "makeblastdb ${filename} failed"
	    fi
	done


	for filename in "${Icescreen_root_dir}"/icescreen_detection_SP/database/hmmdb/FP_profiles/*.hmm; do
	    if [ -f "${filename}" ]; then
		hmmpress -f "${filename}"
		exit_if_error $? "hmmpress ${filename} failed"
	    fi
	done


	for filename in "${Icescreen_root_dir}"/icescreen_detection_SP/database/hmmdb/SP_profiles/*.hmm; do
	    if [ -f "${filename}" ]; then
		hmmpress -f "${filename}"
		exit_if_error $? "hmmpress ${filename} failed"
	    fi
	done
	conda_deactivate_icescreen_env "${galaxy_code_sent}"
}


function test_installation(){

        local galaxy_code_sent=$1
	conda_activate_icescreen_env "${galaxy_code_sent}"
        
	# test compile makeblastdb
	ls "${Icescreen_root_dir}/icescreen_detection_SP/database/blastdb/" | grep '.psq' > /dev/null
	exit_if_error $? "test compile makeblastdb failed"

	# test compile HMMPress
	ls "${Icescreen_root_dir}/icescreen_detection_SP/database/hmmdb/FP_profiles/" | grep '.h3m' > /dev/null
	exit_if_error $? "test compile HMMPress FP_profiles failed"
	ls "${Icescreen_root_dir}/icescreen_detection_SP/database/hmmdb/SP_profiles/" | grep '.h3m' > /dev/null
	exit_if_error $? "test compile HMMPress SP_profiles failed"

	# test modules
	# "${Icescreen_root_dir}/icescreen_detection_ME/runTests.sh"
	"${Icescreen_root_dir}/icescreen_detection_ME/runTests.sh" 2>&1 | grep 'Tests successful' > /dev/null
	exit_if_error $? "test icescreen_detection_ME/runTests.sh failed"
	# "${Icescreen_root_dir}/test_pipeline/runTests.sh" 2>&1 | grep 'Tests successful' > /dev/null
	# exit_if_error $? "test test_pipeline/runTests.sh failed"

	# test most dependencies are installed
	snakemake --version 2>&1 > /dev/null
	exit_if_error $? "test dependencies snakemake failed"
	blastp -version 2>&1 > /dev/null
	exit_if_error $? "test dependencies blastp failed"
	phmmer -h 2>&1 > /dev/null
	exit_if_error $? "test dependencies phmmer failed"
	pip3 show pandas 2>&1 > /dev/null
	exit_if_error $? "test dependencies pandas failed"
	pip3 show biopython 2>&1 > /dev/null
	exit_if_error $? "test dependencies biopython failed"
	# testing of bcbio-gff is skipped : from BCBio import GFF ; print(GFF.__version__)
	conda_deactivate_icescreen_env "${galaxy_code_sent}"

	echo "The installation of ICEscreen is successful"

}


function conda_activate_icescreen_env(){
        local galaxy_code_sent=$1
        if [[ "${galaxy_code_sent}" -eq 0 ]]
        then
		# if [ -d "${CONDA_BASE}/envs/icescreen_env" ]
		if [[ $(conda info --envs | grep 'icescreen_env_1-3-3') ]]
		then
		    ## Then source conda.sh
		    CONDA_BASE=$(conda info --base)
		    source "${CONDA_BASE}/etc/profile.d/conda.sh"

		    ## Now activate environments
		    printf "\nconda activate icescreen_env_1-3-3\n"
		    conda activate icescreen_env_1-3-3
		    exit_if_error $? "conda activate failed"
		else
		    printf "\nWARNING: not automatically activating icescreen_env_1-3-3 via conda because it is not found via conda info --envs.\n"
		fi
	else
		printf "\nWARNING: not automatically activating icescreen_env_1-3-3 via conda because galaxy_code_sent is ${galaxy_code_sent}.\n"
        fi
}
    
    
function conda_deactivate_icescreen_env(){
        local galaxy_code_sent=$1
        if [[ "${galaxy_code_sent}" -eq 0 ]]
        then
		# if [ -d "${CONDA_BASE}/envs/icescreen_env" ]
		if [[ $(conda info --envs | grep 'icescreen_env_1-3-3') ]]
		then
		    ## Then source conda.sh
		    CONDA_BASE=$(conda info --base)
		    source "${CONDA_BASE}/etc/profile.d/conda.sh"

		    ## Now deactivate environments
		    printf "\nconda deactivate\n"
		    conda deactivate
		    exit_if_error $? "conda deactivate failed"
		else
		    printf "\nWARNING: not automatically deactivating icescreen_env_1-3-3 via conda because it is not found via conda info --envs.\n"
		fi
	else
		printf "\nWARNING: not automatically deactivating icescreen_env_1-3-3 via conda because galaxy_code_sent is ${galaxy_code_sent}.\n"
        fi
}


function print_version_dependencies(){
        local galaxy_code_sent=$1
	conda_activate_icescreen_env "${galaxy_code_sent}"
	echo " - icescreen"
	version
	echo " - python"
	python --version
	echo " - pandas"
	pip3 show pandas | grep 'Version:'
	echo " - biopython"
	pip3 show biopython | grep 'Version:'
	# echo " - bcbio-gff"
	# testing of bcbio-gff is skipped : from BCBio import GFF ; print(GFF.__version__)
	# python -c "from BCBio import GFF ; print(GFF.__version__)"
	echo " - snakemake-minimal"
	snakemake --version
	echo " - blastp"
	blastp -version | grep 'blastp:'
	echo " - phmmer"
	phmmer -h | grep 'HMMER'
	conda_deactivate_icescreen_env "${galaxy_code_sent}"
}


########################################
# VARIABLES INITIALIZATION
########################################

# Initialize all option variables.
# This unsures we are not contaminated by variables from the environment.
galaxy=0
gbdir=
outdir=${HOME}
name=
cores=1
mode="" # mode is legacy name for phylum.
deprecation_warnings_to_print=""

Icescreen_Executable_Path="${BASH_SOURCE[0]}"
while [ -h "${Icescreen_Executable_Path}" ]; do # resolve ${Icescreen_Executable_Path} until the file is no longer a symlink
  Icescreen_root_dir=$( cd -P "$( dirname "${Icescreen_Executable_Path}" )" >/dev/null 2>&1 && pwd )
  Icescreen_Executable_Path=$(readlink "${Icescreen_Executable_Path}")
  [[ "${Icescreen_Executable_Path}" != /* ]] && Icescreen_Executable_Path="${Icescreen_root_dir}/${Icescreen_Executable_Path}" # if ${Icescreen_Executable_Path} was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
Icescreen_root_dir=$( cd -P "$( dirname "${Icescreen_Executable_Path}" )" >/dev/null 2>&1 && pwd )
#echo "${Icescreen_Executable_Path}"
#echo "${Icescreen_root_dir}"

########################################
# ARGUMENTS PARSING
########################################

# If there is no arguments given, show help.
if [ $# -eq 0 ]
then
    usage
    exit 0
fi

# Catch all arguments given and check if they are empty.
#while :; do
while test $# -gt 0; do
    case "$1" in
        -h|-\?|--help)
            usage       # Show help.
            exit 0
            ;;
        -v|--version)
            version       # Show version.
            exit 0
            ;;
        --install_dependencies)
            install_dependencies
            exit 0
            ;;
        --print_version_dependencies)
            print_version_dependencies 0
            exit 0
            ;;
        --index_genomic_resources)
            index_genomic_resources 0
            exit 0
            ;;
        --test_installation)
            test_installation 0
            exit 0
            ;;
        --galaxy)
            galaxy=1
            ;;
        -g|--gbdir)
            if [ "$2" ]; then
                gbdir=$2
                shift
            else
                die 'ERROR: '$1' requires a non-empty option argument.'
            fi
            ;;
        --gbdir=?*)
            gbdir=${1#*=}
            ;;
        --gbdir=)
            die 'ERROR: "--gbdir" requires a non-empty option argument.'
            ;;
        -n|--name)
            if [ "$2" ]; then
                name=$2
                shift
            else
                die 'ERROR: '$1' requires a non-empty option argument.'
            fi
            ;;
        --name=?*)
            name=${1#*=}
            ;;
        --name=)
            die 'ERROR: "--outdir" requires a non-empty option argument.'
            ;;
        -o|--outdir)
            if [ "$2" ]; then
                outdir=$2
                shift
            else
                die 'ERROR: '$1' requires a non-empty option argument.'
            fi
            ;;
        --outdir=?*)
            outdir=${1#*=}
            ;;
        --outdir=)
            die 'ERROR: "--outdir" requires a non-empty option argument.'
            ;;
        -j|--jobs)
            if [ "$2" ]; then
                cores=$2
                shift
            else
                die 'ERROR: '$1' requires a non-empty option argument.'
            fi
            ;;
        --jobs=?*)
            cores=${1#*=}
            ;;
        --jobs=)
            die 'ERROR: "--jobs" requires a non-empty option argument.'
            ;;
        -p|--phylum)
            if [ "$2" ]; then
                mode=$2
                shift
            else
                die 'ERROR: '$1' requires a non-empty option argument.'
            fi
            ;;
        --phylum=?*)
            mode=${1#*=}
            ;;
        --phylum=)
            die 'ERROR: "--phylum" requires a non-empty option argument.'
            ;;
        -m|--mode)
            if [ "$2" ]; then
                mode=$2
                deprecation_warnings_to_print="${deprecation_warnings_to_print}\n - [NAME CHANGE] -m|--mode has been replaced by -p|--phylum. -m|--mode will not be supported in a future release."
                shift
            else
                die 'ERROR: '$1' requires a non-empty option argument.'
            fi
            ;;
        --mode=?*)
            mode=${1#*=}
            deprecation_warnings_to_print="${deprecation_warnings_to_print}\n - [NAME CHANGE] -m|--mode has been replaced by -p|--phylum. -m|--mode will not be supported in a future release, please use -p|--phylum instead."
            ;;
        --mode=)
            die 'ERROR: "--mode" requires a non-empty option argument. [NAME CHANGE] -m|--mode has been replaced by -p|--phylum. -m|--mode will not be supported in a future release, please use -p|--phylum instead.'
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARNING: Unknown option (ignored): %s\n' "$1" >&2
            die
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
            ;;
    esac

    shift
done



# Check all required arguments (gbdir).
# Counter of valid required arguments (must be equal to 1).
counter=0

if [ -z "${gbdir}" ]
then
    die 'ERROR: Genbank folder (-g, --gbdir) is required'
else
    if [ -d "${gbdir}" ]
    then
        counter=$((counter+1))
    else
        die 'ERROR: Genbank folder '"${gbdir}"' does not exist'
    fi
fi


#trim trailing slashes
gbdir=$(echo "${gbdir}" | sed 's/\/*$//g')
outdir=$(echo "${outdir}" | sed 's/\/*$//g')

gbdir=$(get_abs_filename "${gbdir}")
outdir=$(get_abs_filename "${outdir}")


# mode lowercase
mode=$(echo ${mode} | tr '[:upper:]' '[:lower:]')

# Check parameters for the Taxonomy mode
if [ -z "${mode}" ]
then
   # $mode is empty   
    mode="bacillota" # mode is legacy name for phylum. Soon will be no defaut value anymore. 
    deprecation_warnings_to_print="${deprecation_warnings_to_print}\n - [SOON TO BE MANDATORY ARGUMENT] -p|--phylum will soon be a mandatory arguments for running icescreen. Running icescreen without -p|--phylum will not be supported in a future release. For this version, --phylum is set to \"bacillota\" by default."
    # die "ERROR: Option \"-p|--phylum\" is now mandatory. Supported phylum: \"bacillota\".";
fi

if [ "${mode}" == "firmicutes" ]
then
   # $mode is empty   
    mode="bacillota" # firmicutes is the old name for bacillota. 
    deprecation_warnings_to_print="${deprecation_warnings_to_print}\n - [NAME CHANGE] \"firmicutes\" has been replaced by \"bacillota\" to keep up with the current taxonomy nomenclature."
fi

modepath="${Icescreen_root_dir}/icescreen_pipelines/mode/${mode}.yml"
if [ ! -f "${modepath}" ];
then
    die "ERROR: phylum ${mode} is not supported. Supported phylum: \"bacillota\".";
fi

if [ -n "${deprecation_warnings_to_print}" ]
then
   # $deprecation_warnings_to_print is not empty   
    printf "\n\n**DEPRECATION WARNINGS**: $deprecation_warnings_to_print\n\n"
fi

if [[ "$counter" -eq 1 ]]                             # Check if required argument is valid.
then
    #echo "All required parameters are valid"
    if [ -d "${outdir}" ]                               # If ${outdir} is valid, display all arguments that will be used by the program.
    then
        # Display input of user
        printf "\nInput arguments:\n"
        printf "\tPhylum:                               "${mode}"\n"
        printf "\tGenbank folder:                       "${gbdir}"\n"
	
        if [ -z "${name}" ]
        then
            # If there is no name for the set
            # outdir="${outdir}/ICEscreen_results/"
            outdir="${outdir}/ICEscreen_results"
        else
            outdir="${outdir}/ICEscreen_results/${name}"
        fi
        printf "\tICEscreen output:                     ${outdir}\n"

        # Create folder with ICEscreen folder output if it does not already exist
        # and create a file with the path of source genbank
        mkdir -p ${outdir}

        # Get folder of running script
        #src=$(dirname $(realpath -s $0))

        mkdir -p "${outdir}/genbank"
        mkdir -p "${outdir}/faa"
        mkdir -p "${outdir}/results"
        #for x in ${gbdir%/}"/"*
        printf "\nGenbank file(s) registred to be analysed:\n"
        for x in ${gbdir}"/"*
        do
		# exclude subdirectories
		if [ -f "${x}" ]; then
			printf "\t- "${x}"\n"
        
			y=$(basename ${x})
			sanitizedname=$(echo ${y%*.*}.gb | sed -e 's/[^A-Za-z0-9._-]/_/g')
			if [ ! -f "${outdir}/genbank/${sanitizedname}" ]
			then
				gborig=$(realpath ${x})
				ln -s "${gborig}" "${outdir}/genbank/${sanitizedname}"
				mkdir -p "${outdir}/results/${sanitizedname%*.*}"
				# Add the config file for each genbank
				cat "${Icescreen_root_dir}/icescreen_pipelines/icescreen_config_template.yml" <( tail -n+2 ${modepath} | awk 'BEGIN{print "### PARAMETERS USED ###"}1' | awk 'BEGIN{print "---"}1' ) > "${outdir}/results/${sanitizedname%*.*}/icescreen.conf"
				# Initialize config parameters
				bash "${Icescreen_root_dir}/icescreen_pipelines/edit_configfile.sh" --conffile "${outdir}/results/${sanitizedname%*.*}/icescreen.conf" --gbfile "${gborig}"
			fi
		else
			printf "WARNING: "${x}" has been skipped and the Genbank file(s) in it will not be analysed.\n"
		fi
        done

	conda_activate_icescreen_env "${galaxy}"
        # if [[ "${galaxy}" -eq 0 ]]
        # then
	# 	CONDA_BASE=$(conda info --base)
	# 	if [ -d "${CONDA_BASE}/envs/icescreen_env" ]
	# 	then
	# 	    ## Then source conda.sh
	# 	    source "${CONDA_BASE}/etc/profile.d/conda.sh"
	# 	    ## Now activate environments
	# 	    printf "\nconda activate icescreen_env\n"
	# 	    conda activate icescreen_env
	# 	    exit_if_error $? "conda activate failed"
	# 	fi
        # fi


        # Run ICEscreen pipeline
        snakemake --snakefile "${Icescreen_root_dir}/icescreen_pipelines/icescreen.snakefile" --config rootdir="${Icescreen_root_dir}" gbdir="${outdir}/genbank" outdir="${outdir}" mode="${modepath}" -j ${cores} --nolock
	exit_if_error $? "Running icescreen with snakemake failed"

        # Remove genbank folder
        rm -rf "${outdir}/genbank"
        rm -rf "${outdir}/results"
        rm -rf "${outdir}/faa"

	conda_deactivate_icescreen_env "${galaxy}"
        # if [[ "${galaxy}" -eq 0 ]]
        # then
	# 	CONDA_BASE=$(conda info --base)
	# 	if [ -d "${CONDA_BASE}/envs/icescreen_env" ]
	# 	then
	# 	    ## Then source conda.sh
	# 	    source "${CONDA_BASE}/etc/profile.d/conda.sh"
	# 	    ## Now deactivate environments
	# 	    printf "\nconda deactivate\n"
	# 	    conda deactivate
	# 	    exit_if_error $? "conda deactivate failed"
	# 	fi
        # fi


    else
        die "ERROR: Path for output ${outdir} does not exist."
    fi
else
    die "ERROR: Not all required parameters are valid."
fi


if [ -n "${deprecation_warnings_to_print}" ]
then
   # $deprecation_warnings_to_print is not empty   
    printf "\n\n**DEPRECATION WARNINGS**: $deprecation_warnings_to_print\n\n"
fi

